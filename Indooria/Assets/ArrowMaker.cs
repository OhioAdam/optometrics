using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ArrowMaker : MonoBehaviour, ITrackableEventHandler
{
  public GameObject arrow;
  public float positionX;
  public float positionY;
  // Use this for initialization
  private TrackableBehaviour mTrackableBehaviour;

  void Start()
  {
    Debug.Log("Test1");
    mTrackableBehaviour = GetComponent<TrackableBehaviour>();
    if (mTrackableBehaviour)
    {
      mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }
  }

  public void OnTrackableStateChanged(
                                  TrackableBehaviour.Status previousStatus,
                                  TrackableBehaviour.Status newStatus)
  {

    if (newStatus == TrackableBehaviour.Status.DETECTED ||
        newStatus == TrackableBehaviour.Status.TRACKED ||
        newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
    {
      if (GameObject.Find("Building(Clone)") != null)
      {
        Destroy(GameObject.Find("Building(Clone)"));
      }

      GameObject parentObj = GameObject.Find("Origin");
      parentObj.GetComponent<Transform>().position= transform.position+new Vector3(positionX,0,positionY);
      parentObj.GetComponent<Transform>().rotation= new Quaternion(transform.rotation.x,0, transform.rotation.z,0);
      GameObject floorplan = Instantiate(arrow, parentObj.transform.position, parentObj.transform.rotation);
    }
  }
}
